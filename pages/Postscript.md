.. title: Postscript

The Post Office collective is involved in developing socio-technical workflows, starting from our own position of agency - in knowledge production, scholarly publishing, access to knowledge, and processes of political pedagogy. This is early work in progress, so stay tuned, but here is something to give you an idea:

1. [Scholar-led publishing](http://radicaloa.disruptivemedia.org.uk/philosophy/)
2. [Amateur librarianship](https://knowledge-production.github.io/forge/tutorials/book-digitization.html)
3. [Online syllabi of social movements](https://issuu.com/instituteofnetworkcultures/docs/statemachines_v14_zondermarks/116)

<br>
<br>
---
