.. title: Delivered the Same Day: The Post Office and Amazon.com
.. date: 2019-05-08 13:37
.. slug: delivered-the-same-day
.. author: ...

# Seminar Series: Work, Property, Metrics

**Venue:** Coventry University, [Lanchester Library](https://goo.gl/maps/D7pwN1KuEyzzweN57), Teaching Room, 3rd floor  
**Time:** *June 3rd, 2019, 11:00-17:00*


> **Guest Speakers**: !Mediengruppe Bitnik, Jamie Woodcock  
> **CPC researchers**: Janneke Adema, Peter Conlin, Valeria Graziano, Gary Hall, Kaja Marczewska, Marcell Mars, Tomislav Medak

## Delivered the Same Day: The Post Office and Amazon.com

Historically the postal service has solidified as a national and transnational infrastructure owned by the public, for the benefit of the public. It has played a central role in organising long-distance communication - mail, telegraph, telephony - facilitating, amongst other things, the regular delivery of written correspondence, the dissemination of printed matter, the expansion of the public sphere, but also the surveillance of communication and the coordination of war.

Alongside being political, the postal service was also an intensely technological institution. It has laid down the postal paths, network topologies, and routing protocols for later communications networks to follow. In fact, in the UK, the Post Office has had its own Research Station since 1909, helping develop the first transatlantic radiotelephone service and the world's first programmable electronic digital computer – Colossus.

No less important is the fact that the postal service also created a material distribution system. In the very period when the communicational infrastructures became deregulated, privatised and disinvested, on the back of these services and their delivery networks arose the digital platforms - most prominently the informational-distributive Behemoth that is the Amazon.com.

This event will explore the contrast between the transformative role the postal service as a public institution has had - and continues to have in spite of its creeping privatisation and erosion - and the disruptive role Amazon.com is now playing by offering cloud computing infrastructure, furthering platformisation, automating digital network services, and casualising the labour inside and outside of of its own operations.

**Work, property, metrics** is a series of seminars, workshops, and talks investigating the transformations of work, property relations, and mechanisms of social control resulting from the processes of digitisation, computerisation, and automation. These transformations are reflected in a crisis of institutions responsible for the universal access to social goods and services: employment, care, education, housing etc. With our research activities we want to investigate, support, and develop practices emerging in response to this crisis - ranging from alternative organisational models, collective forms of work, all the way to refusal. In its first round of events, this series is focusing on the institutions responsible for the provision of communication, information, and education - the postal service, the library, and the university.

## Program

<span style="opacity: 0;">................. </span>| I. Post Post Office: the Afterlives of Postal Networks
-------------|----------------------------------------------------------------------------
 11:00-11:15 | **Gary Hall**: introducing the Post Office
 11:15-12:00 | **!Mediengruppe Bitnik**: Postal Machine Decision (artist talk)
 12:00-12:45 | **Kaja Marczewska**: Postal Reforms and the Crisis of Grassroots Publishing
             | **Janneke Adema & Kaja Marczewska**: Post-it. Academic Post Cards

<span style="opacity: 0;">................. </span>| II. Amazon.com: work, property and metrics in the logistical circuit
-------------|----------------------------------------------------------------------------
 13:30-13:45 | **Valeria Graziano**: Introduction to work, property, metrics
 13:45-14:15 | **Marcell Mars & Tomislav Medak**: The Anatomy of Amazon.com
 14:15-14:45 | **Jamie Woodcock**: Amazon.com, Twitch.tv, and Videogames
 14:45-15:15 | **Peter Conlin**: Incontrovertible Landscapes and Vanishing Points: Logistic Spaces, Opacity and Obscurity
 15:30-16:30 | Discussion

## Speakers

**[Janneke Adema](https://openreflections.wordpress.com/)** is a research fellow at the Centre for Postdigital Cultures, Coventry University. In her research, she explores the future of scholarly communications and experimental forms of knowledge production, where her work incorporates processual and performative publishing, radical open access, scholarly poethics, media studies, book history, cultural studies, and critical theory. She explores these issues in depth in her various publications, but also by supporting a variety of scholar-led, not-for-profit publishing projects, including the Radical Open Access Collective, Open Humanities Press, and Post Office Press (POP). You can follow her research, as it develops, on openreflections.wordpress.com.

**[!Mediengruppe Bitnik](https://wwwwwwwwwwwwwwwwwwwwww.bitnik.org/)** are the artists Carmen Weisskopf and Domagoj Smoljo. They are contemporary artists working on and with the Internet. Their practice expands from the digital to affect physical spaces, often intentionally applying loss of control to challenge established structures and mechanisms. In early 2013 !Mediengruppe Bitnik sent a parcel to WikiLeaks founder Julian Assange at the Ecuadorian embassy. The parcel contained a camera which broadcast its journey through the postal system live on the internet. They describe «Delivery for Mr. Assange» as a SYSTEM_TEST and a Live Mail Art Piece. They have also been known for sending a bot called «Random Darknet Shopper» on a three-month shopping spree in the Darknets where it randomly bought objects like Ecstasy and had them sent directly to the gallery space.

**Peter Conlin** is a media lecturer and research associate at the Centre for Postdigital Cultures, Coventry University. He is currently developing a book project entitled *Evitable: The De-obsolescent Future of Media and Urban Space* (Routledge Press).

**Valeria Graziano** is a research associate at the Centre for Postdigital Cultures, Coventry University. My research looks at the organization of cultural practices that foster the refusal to work and the possibility of political pleasure. She is co-editor with Kim Trogal of ‘Repair Matters’, a special issue of *ephemera: Theory & Politics in Organisation* (2019) and convenor of the international project [Pirate Care](https://piratecare.net).

**[Gary Hall](http://www.garyhall.info/)** is a writer, philosopher and cultural theorist working (and making) in the areas of digital media, politics and technology. He is Professor of Media in the Faculty of Arts & Humanities at Coventry University, UK, where he co-directs the Centre for Postdigital Cultures. He is the author of a number of books, including *The Inhumanist Manifesto* (Techne Lab, 2017), *Pirate Philosophy* (MIT Press, 2016), *The Uberfication of the University* (Minnesota UP, 2016), *Digitize This Book!* (Minnesota UP, 2008), and *Culture in Bits* (Continuum, 2002).

**[Marcell Mars](http://ki.ber.kom.uni.st)** is a research associate at the Centre for Postdigital Cultures, Coventry University. Mars is one of the founders of Multimedia Institute/MAMA in Zagreb. His research “Ruling Class Studies”, started at the Jan van Eyck Academy (2011), examines state-of-the-art digital innovation, adaptation, and intelligence created by corporations such as Google, Amazon, Facebook, and eBay. He is a doctoral student at Digital Cultures Research Lab at Leuphana University, writing a thesis on “Foreshadowed Libraries”. Together with Tomislav Medak he founded Memory of the World/Public Library, for which he develops and maintains software infrastructure.

**[Tomislav Medak](http://tom.medak.click)** is a doctoral student at the Centre for Postdigital Cultures, Coventry University. Medak is a member of the theory and publishing team of the Multimedia Institute/MAMA in Zagreb, as well as an amateur librarian for the Memory of the World/Public Library project. His research focuses on technologies, capitalist development, and postcapitalist transition, particularly on economies of intellectual property and unevenness of technoscience. Together with Marcell Mars he coedited Public Library and Guerrilla Open Access.

**[Jamie Woodcock](https://www.jamiewoodcock.net/)** is a researcher at the Oxford Internet Institute, University of Oxford. He is the author of *Marx at the Arcade* (2019, Haymarket) about videogames, and *Working The Phones* (2017, Pluto), a study of a call centre in the UK - both inspired by the workers' inquiry. His research focuses on labour, work, the gig economy, platforms, resistance, organising, and videogames. He is on the editorial board of *Notes from Below* and *Historical Materialism*.
