.. title: Experimental Publishing II - Critique, Intervention, And Speculation
.. date: 2019-05-01 22:44
.. slug: experimental-publishing-2
.. author: ...

**Experimental Publishing II – Critique, Intervention, And Speculation**  

A half-day symposium with talks by Mark Amerika (UC Boulder) and Nick Thurston (University of Leeds)

![](https://www.post-publishing.org/wp-content/uploads/2019/04/postpublishing-2-poster-724x1024.png)
**2:15-5:30pm May 28**  
**Centre for Postdigital Cultures**  
**Teaching Room**  
**3rd Floor Lanchester Library**  
**Coventry University**  
**Registration (free):** [https://www.coventry.ac.uk/research/about-us/research-events/2019/experimental-publishing-ii2/](https://www.coventry.ac.uk/research/about-us/research-events/2019/experimental-publishing-ii2/)
  
In 2019 and 2020, the Centre for Postdigital Cultures (CPC) will be hosting a series of symposia exploring contemporary approaches to experimental publishing. Over the course of the series, we will ask questions about the role and nature of experimentation in publishing, about ways in which experimental publishing has been formulated and performed in the past, and ways in which it shapes our publishing imaginaries at present. This series aims to conceptualise and map what experimental publishing is or can be and to explore what lies behind our aims and motivations to experiment _through_ publishing. As such, it forms the first activity within the CPC’s new [Post-Publishing programme](https://post-publishing.org/), an initiative committed to exploring iterative and processual forms of publishing and their role in reconceptualising publishing as an integral part of the research and writing process, i.e. as that which inherently shapes it.

**Speakers**

[Mark Amerika](http://markamerika.com/), a Professor of Distinction at the University of Colorado, has exhibited his artwork internationally at venues such as the Whitney Biennial of American Art, the Institute of Contemporary Arts in London, and The ZKM | Center for Art and Media in Karlsruhe, Germany.  He is the author of many books including _The Kafka Chronicles_ (FC2), _Sexual Blood_ (FC2), _remixthebook_ (University of Minnesota Press—[remixthebook.com](https://eur01.safelinks.protection.outlook.com/?url=http%3A%2F%2Fwww.remixthebook.com%2F&data=02%7C01%7Cac8699%40coventry.ac.uk%7C18750fedb8cd44b2a07c08d6cf0e32b1%7C4b18ab9a37654abeac7c0e0d398afd4f%7C0%7C1%7C636924057228008228&sdata=KtibnIQIoTPIjTZEDMdx2JBDpedyVSl5CwKOdoMkqss%3D&reserved=0)), _META/DATA: A Digital Poetics_ (The MIT Press), _remixthecontext_ (Routledge), and _Locus Solus (An Inappropriate Translation Composed in a 21st Century Manner)_ (Counterpath Press).

[Nick Thurston](https://eur01.safelinks.protection.outlook.com/?url=https%3A%2F%2Fwww.nickthurston.info%2F&data=02%7C01%7Cac8699%40coventry.ac.uk%7C861725ba438d4bca719508d6ce809c7d%7C4b18ab9a37654abeac7c0e0d398afd4f%7C0%7C0%7C636923449130244052&sdata=4CBXRgQyCt4Mgg7i7upYIoIWTWFVmWMLbCw8icE9VB4%3D&reserved=0) is a writer and editor who makes artworks. His most recent books include the co-edited collection, _Post-Digital Cultures of the Far Right_ (Bielefeld: Transcript, 2018), and an experimental Spanish-language translation of his last poetic book by NO_LIBROS (Barcelona, 2019). Recent and current exhibitions include shows at Transmediale (Berlin, 2018), Q21 (Vienna, 2018), MuHKA (Antwerp, 2018) and HMKV (Dortmund, 2019).

**Concept**

Experimental publishing can be positioned as an intervention, a mode of critique, and a tool of speculation. It is a way of thinking about writing and publishing today that has at its centre a commitment to questioning and breaking down distinctions between practice and theory, criticality and creativity, and between the scholarly and the artistic.

In this series of events we propose to explore contemporary approaches to experimental publishing as:

-   _an ongoing critique_ of our current publishing systems and practices, deconstructing existing hegemonies and questioning the fixtures in publishing to which we have grown accustomed—from the book as a stable object to single authorship and copyright.
-   _an affirmative practice_ which offers means to re-perform our existing writerly, research, and publishing institutions and practices _through_ publishing experiments.
-   _a speculative practice_ that makes possible an exploration of different futures for writing and research, and the emergence of new, potentially more inclusive forms, genres, and spaces of publishing, open to ambivalence and failure.

  
This take on experimentation can be understood as a heterogeneous, unpredictable, and uncontained _process_, one that leaves the critical potentiality of the book as a medium open to new intellectual, political, and economic contingencies.
