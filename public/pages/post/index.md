.. title: Post Office in context


![](/images/Colossus.jpg)
*Photo: A Colossus Mark 2 code-breaking computer being operated by Dorothy Du Boisson and Elsie Booker, 1943.*




# The Notion of Post- in Theory

The research we undertake under the name of Post Office combines several theoretical strands and ideas around the prefix post-, which have been circulating in critical theoretical discourses, from postmodernism to postcolonialism and, more recently, featuring in new fields of debates such as the post-humanities and post-digital cultures.

 - Post-digital
 - Post-humanities
 - Post-human
 - Post-work
 - Post-publishing
 - Post-theory
 - Post-normal science
 - Post-university
 - Post-capitalism

Post- here does not denote an ‘after’ or a distancing. Post- is not set up in an opposition to what precedes it, but can be perceived as deconstructing its assertions in a continuous manner, as a form of radical and ongoing self-critique. The post-humanities, post-digital or post-human have always already been a part of the proclaimed otherness of the humanities, digital, or human.



# Postal Service as a Foundational Communication Network

The name Post Office is moreover a nod to the contested history and potential of one of the key institutions of modern communication.  

Postal services as a system of diffusion of messages emerged as paper replaced stone, as the written document entrusted to a courier replaced the word conveyed by a trusted messenger. While pre- and nonmodern societies have known a number of postal systems, including the formidable network of postal stations set up by the Mongol Empire in the 12th century, it is with the development of modern states that the postal service becomes a government monopoly and public utility. In the 17th century, governments needed a system to facilitate and control the exchange between their subjects as literacy spread (Jeffreys-Jones 2017). When the UK Post Office was first set up by Oliver Cromwell as a state monopoly in 1657, it was as a means of surveillance: the opening of mail was thought to be ‘the best means to discover and prevent any dangerous and wicked designs against the Commonwealth’ (Society for the Diffusion of Useful Knowledge 1834, p. 35).

In the 18th century, dissemination networks setup by the literary underground conspiring against the Ancien Regime (Darnton 1982) gave an ideological imprint to the post-Revolutionary establishment of the public postal service, where society-wide private communication of letters and the public dissemination of print materials became the foundation of civic, cultural, and political life (Gallagher 2017) and consolidated nations as imagined communities (Anderson 2006). And finally since the 19th century, the emerging industrial capitalism required a system of control and coordination over geographically expanding production and distribution (Beniger 1989).

From the confluence of these processes, the postal service solidified as a national and transnational communication network and a communication infrastructure owned by the public for the benefit of the public, laying down the physical routes, network topologies and routing paradigms for later communications networks. Since its inception, the Post Office has been an intensely technological and political public institution.

For instance in the UK, The Post Office Research Station was established in 1909 as a separate section of the General Post Office. Here, investigations of a purely experimental nature were pursued in relation to the telephone and telegraph services and electrical communication more generally. The Post Office Research Station moved to Dollis Hill in north London in 1921, becoming one of the largest and most significant sites for technological innovation by the modern British state, responsible for a host of technological breakthroughs: they introduced the speaking clock; they developed the Trans-Atlantic telephone cable and the first transatlantic radiotelephone service; and in 1943 they build the world's first programmable electronic digital computer – Colossus – which was used at Bletchley Park during the Second World War as a code-breaking computer.

In time, the technology that was (in part) developed out of the Post Office would eventually evolve to eclipse its central role in the dissemination of messages. The disaggregation of telephone and postal services and the rise of digital networks in the 90s deeply modified the status of this modern institution. Yet, the exchange of digital messages is not immaterial, but it relies on the concrete communication infrastructures first imagined as part of global postal services networks. Information coordinates production and distribution, flows of energy and matter. Thus, it is of little surprise that sending parcels and sending messages continue to be entangled. Ironically, even Amazon.com, the Internet giant with the largest valuation as of today and arguably the company playing the biggest transformative role in global material flows, got off to a start only because it could rely - as it continues to do -  on postal services. As postal services are presently undergoing privatisations, restructurings, and rollbacks, they continue to provide a fundamental service without which the digital economy could not operate.


# The Post- as Mess Media Theory

Media theory always faces a dilemma with respect to what route to take: will its object of analysis be the technological infrastructure of messaging in a society or will its object of analysis be the impact of messaging on society. Will it do materialist media archaeology or sociology and political economy of media? Will it be closer to engineering or critique? There is no resolution - and that absence of resolution is a productive conjuncture for research. Media are messy (and leaky - see Chun 2008), not out of mere hybridity or entanglement, but out of the inassimilable encounter of persistent forms (Levine 2015) - conflicting technological formalisations and social institutions.

Technological transformations unleashed by communication networks - digitisation, computerisation and automation - in the present are challenging the continued existence of some of the fundamental institutions of modern societies responsible for universal access to social goods: employment, care, education, housing, etc. In those transformations, technologies are anything but apolitical. A technological design choice - such as that of the distributed allocation of a packet switching network (TCP/IP) as opposed to the centralised allocation of radio frequencies to radio stations - can overturn scarcity into abundance. And yet, the resulting condition, proto-political, still requires political struggle over institutional arrangements. Commoning or enclosing: more equal or more unequal, more centralised or more autonomous, more managerial or more cooperative.

With our research activities we want to investigate, support, and develop practices emerging in response to the crisis of institutions - ranging from alternative organisational models and collective modes of working to refusal. There is no easy way to square technological formalisations and social institutions - no easy, certain and unambiguous recipes for action. In that sense, the Post Office is a performative intervention concerned with creating masked means of intervention that are not necessarely easy to read since, following ‘The Masked Philosopher’ interview by Foucault, 'a name' sometimes 'makes reading too easy'.

---

Anderson, B. (2006) *Imagined Communities: Reflections on the Origin and Spread of Nationalism*. Verso

Beniger, J. (1989) *The Control Revolution: Technological and Economic Origins of the Information Society*. Harvard University Press

Chun, W.H.K. (2008) *Control and Freedom: Power and Paranoia in the Age of Fiber Optics*. MIT Press

Darnton, R. (1982) *The Literary Underground of the Old Regime*. Harvard University Press

Gallagher, W. (2016) *How the Post Office Created America: A History*. Penguin

Jeffreys-Jones, R. (2017) *We Know All about You: The Story of Surveillance in Britain and America*. Oxford University Press

Levine, C. (2015) *Forms: Whole, Rhythm, Hierarchy, Network*. Princeton University Press

Society for the Dissemination of Useful Knowledge (1834) *The Penny Magazine of the Society for the Diffusion of Useful Knowledge: The History and Present State of the Post Office*. Charles Knight

---
