.. title: Post Office Press


![test](/images/POP.png)

[https://twitter.com/POPostpress](https://twitter.com/POPostpress)

Post Office Press (POP) is a scholar-led experimental and collective press based within Coventry University’s Post Office/Centre for Postdigital Cultures. POP focuses exclusively on the publishing and promotion of experimental academic publications. Furthermore, as a scholar-led and collective press, its aim is to promote independent, not-for profit academic-led publishing and as such to support more diversity in the scholarly publishing landscape—currently dominated by commercial players.

POP will support the development of and the experimentation with alternative forms of scholarly publishing, be they formally challenging or more conceptual in outlook (for example anonymous publications, which can be perceived as a provocation to academia's star system). With its focus on experimentation, POP wants to join a wider movement of scholars and presses committed to exploring new publication practices, ones that take advantage of the limitless possibilities of both print and digital media and their various affordances. POP wants to do so to accommodate scholars’ changing research needs and practices, which are increasingly digital, social, and multimodal. It wants to push the boundaries of what counts as scholarship and academic publication.

POP wants to provide an experimental space, yet it also wants to promote and create a wider ecology that is open to emerging forms, genres and spaces of scholarly practice. By supporting experimentation POP seeks to creatively disrupt and question existing practices in academic publishing, which often remain print-based and focused. Yet at the same time POP wants to create conditions under which any experimentation with the form of our research is not held back by a lack of technological expertise (from either author or press) nor by commercial imperatives—i.e. whether a publication is on trend or has ‘market appeal’.


POP published six pamphlets for the [2nd Radical Open Access conference](http://radicaloa.disruptivemedia.org.uk/conferences/roa2/), which can be found in the repository of [Humanities Commons](https://hcommons.org/members/pop/).
<br>

[*Predatory Publishing*](https://hcommons.org/deposits/item/hc:19827/)
![Predatory Publishing](/images/POP_predatory_publishing.png)
**Authors:** Kirsten Bell, Jill Claassen, Lena Nyahodza, Reggie Raju, Vaclav Stetka<br>
**Editors:** punctum books, Post Office Press<br>
**Permanent URL:** [http://dx.doi.org/10.17613/M6N58CK3D](http://dx.doi.org/10.17613/M6N58CK3D)
<br>

[*Guerilla Open Access*](https://hcommons.org/deposits/item/hc:19825/)
![Guerilla Open Access](/images/POP_guerilla_OA.png)
**Authors:** Laurie Allen, Balázs Bodó, Chris Kelty<br>
**Editors:** Memory of the World (Marcell Mars & Tomislav Medak), Post Office Press<br>
**Permanent URL:** [http://dx.doi.org/10.17613/M6RX93C77](http://dx.doi.org/10.17613/M6RX93C77)
<br>

[*Humane Metrics/Metrics Noir*](https://hcommons.org/deposits/item/hc:19823/)
![Humane Metrics/Metrics Noir](/images/POP_humane_metrics.png)
**Authors:** Martina Franzen, Eileen Joy, Chris Long<br>
**Editors:** meson press, Post Office Press<br>
**Permanent URL:** [http://dx.doi.org/10.17613/M6WP9T61M](http://dx.doi.org/10.17613/M6WP9T61)
<br>

[*Competition and Cooperation*](https://hcommons.org/deposits/item/hc:19821/)
![Competition and Cooperation](/images/POP_competition_cooperation.png)
**Authors:** Maddalena Fragnito, Valeria Graziano, Sebastian Nordhoff<br>
**Editors:** Open Humanities Press, Post Office Press<br>
**Permanent URL:** [http://dx.doi.org/10.17613/M61G0HT64](http://dx.doi.org/10.17613/M61G0HT64)
<br>

[*The Geopolitics of Open*](https://hcommons.org/deposits/item/hc:19819/)
![The Geopolitics of Open](/images/POP_geopolitics_of_open.png)
**Authors:** Denisse Albornoz, George (Zhiwen) Chen, Maggie Huang, Tasneem Mewa, Gabriela Méndez Cota, Ángel Octavio Álvarez Solís<br>
**Editors:** Culture Machine, Post Office Press<br>
**Permanent URL:** [http://dx.doi.org/10.17613/M65717N1C](http://dx.doi.org/10.17613/M65717N1)
<br>

[*The Poethics of Scholarship*](https://hcommons.org/deposits/item/hc:19815/)
![The Poethics of Scholarship](/images/POP_poethics_of_scholarship.png)
**Authors:** Janneke Adema, Kaja Marczewska, Frances McDonald, Whitney Trettien<br>
**Editors:** Post Office Press<br>
**Permanent URL:** [http://dx.doi.org/10.17613/M6DN3ZV67](http://dx.doi.org/10.17613/M6DN3ZV6)

<br>
<br>
---
