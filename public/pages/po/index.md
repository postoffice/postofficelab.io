.. title: Post Office in a few words


Post Office is an exercise in imagining counter-institutions in response to the crisis of those very institutions. It is an endeavour in devising practices that collectively re-configure public infrastructures against the onslaught of managerial neoliberalism and technological acceleration. As a research collective (connected to the [Centre for Postdigital Cultures at Coventry University](http://www.coventry.ac.uk/research/areas-of-research/postdigital-cultures/)), it is a horizontal experiment in knowledge production and action situated between theory and technology, politics and poetics, the inside and the outside of the university.

Post Office follows a methodology of affirmative critique. Our projects are both critical and performative: actively changing the situations in which they intervene while helping devise protagonist-centered approaches to organisation, methodology, and technology. We are involved in changing [scholarly](/pages/postfolio#seizing-the-means) and [creative writing](https://www.bloomsbury.com/uk/this-is-not-a-copy-9781501337833/), [publishing](/pages/postfolio/#post-publishing), [libraries](https://www.memoryoftheworld.org/), [open access](/pages/postfolio#radical-open-access), [universities](https://www.upress.umn.edu/book-division/books/the-uberfication-of-the-university), [cultural production](http://themaintainers.org/blog/2017/11/2/the-return-of-the-repair-shop-between-consumerism-and-social-reproduction), [the humanities](https://disruptivemedia.org.uk/disrupting-the-humanities-towards-posthumanities/), [technologies](https://knowledge-production.github.io/forge/), and [labour relations](/pages/postfolio#work-property-metrics), and want to create alternatives for a more just, diverse, and equitable future.

<br>
<br>
---
