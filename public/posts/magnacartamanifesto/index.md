.. title: The Magna Carta Manifesto: Liberties and Commons for All by Peter Linebaugh
.. date: 2020-07-22 12:00:00 UTC
.. author: ...


# Two Charters (Chapter Two)

> What are thou Freedom?  
> Thou are not, as imposters say,  
> A shadow soon to pass away,  
> A superstition, and a name  
> Echoing from the cave of Fame.  
> For the labourer thou art bread,  
> And a comely table spread.  
> From his daily labour come  
> To a neat and happy home.  
> Thou art clothes, and fire, and food  
> 
> *- P.B. Shelley, The Masque of Anarchy (1819)*

For eight centuries Magna Carta has been venerated. “It was born with a grey Beard,” Samuel Johnson said. The Massachu- setts Body of Liberties (1641), the Virginia Bill of Rights (1776), the Fifth and Fourteenth Amendments to the U.S. Constitution

 ~~ [ 22 ] ~~~

quote its language.[^1] The story of the political and legal rights is known. Indeed it is too well known, inasmuch as it is remem- bered largely as myth and as icon, as part of the foundation of Western civilization. In 1956 Winston Churchill published the first volume of his *History of the English-Speaking Peoples* in which he glorified Anglo-American “brotherhood,” “destiny,” and empire by reverent references to childhood memories of Magna Carta.[^2]

Magna Carta puts an emergency brake on accelerating state despotism. The handle for the brake is chapter 39. The British human rights barrister Geoffrey Robertson writes, “The appear- ance of ‘rights’ as a set of popular propositions limiting the sov- ereign is usually traced to Magna Carta in 1215, although that document had nothing to do with the liberty of individual citizens: it was signed by a feudal king who was feuding with thuggish barons, and was forced to accede to their demands.”[^3] There is no evidence that King John could write. Besides, we must ask *who* traces rights to Magna Carta? There is a conservative interpretation restricting it to the elite, and there is a popular interpretation that includes free people and commoners.

Robertson continues, Magna Carta “contained some felicitous phrases which gradually entered the common law and worked

 ~~ [ 23 ] ~~~

their rhetorical magic down the centuries.” To call “the felicitous phrases” magic is to overlook the struggle in the streets and fields, the struggle in the prisons, the struggle in the slave ships, the struggle in the press, the struggle in parliament. The historian Simon Schama blithely waves a magic wand, “But for once, England didn’t want an Arthur. It had Magna Carta instead. And that, it was hoped, would be Excalibur enough.” Monty Python explains.

> ARTHUR: I am your king.  
> WOMAN: I didn’t know we had a king. I didn’t vote for you.  
> ARTHUR: People don’t vote for king.  
> WOMAN: How did you become king?  
> ARTHUR: The Lady of the Lake. Her arms clad in the purest shimmering samite held aloft Excalibur from the bosom of the water signifying by divine authority that I, Arthur, was to carry Excalibur. That is why I am your king.  
> MAN: Listen. Strange women lying in ponds distributing swords is no basis for a system of government. Supreme executive power derives from a mandate from the masses, not from a farcical aquatic ceremony.  
> ARTHUR: Be quiet.  
> MAN: You can’t expect to wield supreme executive power just because some watery tart threw a sword at you.  
> ARTHUR: Shut up.[^4]

In the middle of June 1215, on a meadow, Runnymede, along the river Thames the rebellious barons and King John promised

 ~~ [ 24 ] ~~~

on oath to be faithful to one another along the lines of the sixty-three chapters of Magna Carta. Behind the event lay powerful forces of pope and emperor, dynastic intrigues of France and En- gland, wicked deeds of pogrom and bigotry in the name of God Almighty, the disintegrating effects of the money economy, and the multifaceted popular defense of the commons.

As we assess the experience of the long twelfth century (cul- minating in 1215), what strikes us is the similarity of global debates with our own in the twenty-first century. In the summer of 2001 it was the call for reparations for the racist exploitation of Africa and the insistence at a mass gathering in Genoa that “another world is possible,” which preceded the “war on terror” so often compared to a modern crusade. Islam replaced Com- munism as the demonized Other in the ideology of the ruling class. The genesis of capitalist society has been pushed back to the Middle Ages, when communistic heretical movements and Islam were the main threats to church and king.[^5]

The Crusades were military diversions from the social and economic conflicts within Europe. Pope Urban II made this clear in his Clermont speech in 1095 when he declared the *bellum sacrum*, or the First Crusade, saying “let those who have been robbers for a long time now become knights.” In the same speech he demonized the Arab and Turkish Muslims: they worship Satan, they torture, they’re filthy, they’re rapists, and, in one of the first racist and genocidal programs of European history, he called on the Christians “to destroy that vile race.” During the Crusades of the next century recruiters attempted to drum up

 ~~ [ 25 ] ~~~

support with visions of a land of milk and honey, and the realization on earth of a harmonious peaceable kingdom.[^6] It was a combination of utopian thinking and genocidal reality that would recur in European and American history.

The forces that caused the violence within Europe during the twelfth century—increased pauperization, intensification of expropriation of serfs, growth of towns, and the emergence of monetary and commercial relations—led, on the one hand, to competing claims of order between centralizing monarchies and the expanding papacy, and on the other hand, to a wide variety of movements from below deemed to be heretical. These movements have been likened to a proto–First International to stress their proletarian character. Cathars, Waldensians, followers of the French pantheist Amalric of Bena, the Fraticelli, the flagellants, the Brethren of the Free Spirit, and followers of Joachim of Fiore had a diverse theological and social program, but all were regarded as threatening by the feudal and church hierarchy. Joachim prophesized a new age, the age of the spirit, when church hierarchy would be unnecessary and when Christians would unite with infidels.

Prophets and messiahs preached the doctrine of having all things in common, which made sense to peasants who resolutely defended their customs and communal routine against the en- croachments of feudal landlords and grasping clergy. The notion of having all things common was made plausible by the network of customary rights and practice on common lands, which

 ~~ [ 26 ] ~~~

already by the thirteenth century was both old and endangered. On the one hand the shortage of arable land led to *assarts* (arable clearings made by grubbing up the trees) in wastes and woodlands, and on the other hand, the intensified pressure in the face of rising prices by the lords on the impoverished peasantry threatened forms of commoning that were essential to small-holders in the thirteenth century.

If crusades against Islam were bids to control the commercial economy of the East, then crusades against heretics were means of terrorizing the landless population of the West. In 1208 the pope launched an exterminating crusade upon the heretics of Albi, in the south of France. Believing that the world around them was diabolical, they opposed procreation as an unkindness. The children of the Children’s Crusade of 1212 were sold into slavery. Meanwhile in England, against John’s will, the pope appointed Stephen Langton archbishop of Canterbury. In 1208 the pope placed King John under interdict and in the following year excommunicated him and his kingdom. The church bells were removed from the steeples, statues of the saints were laid on the ground. King John made up by surrendering his kingdom as a feudal fief to the pope.

In 1214 John’s ambitions in France were dashed at the battle of Bouvines. He lost Normandy, the ancestral homeland of the ruling class of England since the Norman invasion of 1066. Philip of France now looked at England with acquisitive eyes. In February 1215 King John responded by making a vow to lead a crusade to the holy land to take it from the Muslim infidels. Becoming “a warrior of God,” he enjoyed immunities protecting him from the barons. Raising money to recover Normandy and to join the crusade, King John oppressed the barons with *scutage*

 ~~ [ 27 ] ~~~

(a tax paid by a knight in lieu of military service), by stealing forests, by taking children hostage for ransom (he slaughtered the twenty-eight sons of Welsh hostages), and by selling women. He made a regular traffic in the sale of wards, maids of fourteen and widows alike. In 1214 he sold his first wife, Isabella of Gloucester, to Geoffrey de Mandeville for the sum of 20,000 marks.[^7] These oppressions were the direct result of his plans to fight the infidels.

The Fifth Crusade set out in 1215; its principal ideological recruiter was Philip of Oxford. His general argument for “taking up the cross” is that crusading is an exalted vocation imitating Christ. His way of saying so is confused because he uses figures of speech that directly refer to the expropriations of European forest dwellers. “In the beautiful wood of paradise death was hidden under the mantle of life, so, on the contrary, in the deformed and horrible wood life was hidden under the mantle of death, just as life is concealed, in the case of the crusaders, under the mantle of a labor, which is like death.”[^8] Are the woods beautiful or horrible? Are the woods paradise or death? The answer depended on whether you were a baron or a commoner. Crusading was thus a murderous device to resolve a contradiction by bringing baron and commoner together in the cauldron of religious war.

Magna Carta was a document of Christian Europe—its first chapter concerned the freedom of the Christian Church from the

 ~~ [ 28 ] ~~~

secular authority of king. Events in the church and in England ran parallel. The pontificate of Innocent III (1198–1216) corresponded to the reign of John (1199–1216). King John agreed to a five-year truce with al-Adil in 1211, the great Saladin’s brother and his suc- cessor as sultan of Egypt. The pope meanwhile in 1215 opened the fourth Lateran Council, which established the church doctrine of transubstantiation, annual confession, and Easter communion, and which defined heresy. Jews were required to wear identifying badges. It is not a coincidence that the Lateran Council and Magna Carta occurred in the same year. The Lateran Council condemned Joachim of Fiore as a heretic in its second canon and prepared the groundwork of the ruthless Inquisition, a poisonous fungus whose deadly work in an underground, unseen mycelium has spawned racist results for centuries afterward.

In May 1215 the barons took London and withdrew their homage and fealty. In June King John and the barons faced each other in armed camps at Runnymede. The parchment charter of sixty-three chapters of liberties to the “freemen of England” was sealed, and homage renewed viva voce. The charter protected the interests of the church, the feudal aristocracy, the merchants, the Jews, *and* it acknowledged commoners. It assumed a commons. Here we pause in our story in order to summarize some of the leading chapters of the charter.

Its provisions revealed the oppression of women, the aspirations of the bourgeoisie, the mixture of greed and power in the tyranny, an independent ecology of the commons, and the famous chapter 39 from which habeas corpus, prohibition of torture, trial by jury, and the rule of law are derived. “No freeman shall be arrested or imprisoned or dispossessed or outlawed or exiled or any way victimized, neither will we attack him or send anyone to

 ~~ [ 29 ] ~~~

attack him, except by the lawful judgment of his peers or by the law of the land.” The next chapter simply stated, “To no one will we sell, to no one will we refuse or delay right or justice.”

The value of the individual provisions in the eyes of the only contemporary chronicler (a minstrel attached to Robert of Béthune) put first those treating the disparagement of women and the loss of life or member for killing beasts in the forest.[^9]

Chapters 7 and 8 said simply, “A widow shall have her mar- riage portion and inheritance forthwith and without difficulty after the death of her husband.” No widow shall be forced to marry so long as she wishes to live without a husband. We can truly say that “one of the first great stages in the emancipation of women is to be traced” to Magna Carta.[^10] These provisions arose from a grassroots women’s movement that contributed to the construction of alternative models of communal life.[^11]

Magna Carta acknowledged the interests of the urban bourgeoisie. The London commune was established in 1191, and its oath was sworn, unlike the oath of homage, among equals. John was the first king to give a charter to the City of London, with annual election for mayor. The eighteenth-century Scottish philosopher and historian David Hume says that during John’s reign

 ~~ [ 30 ] ~~~

London Bridge was finished in stone. Magna Carta established the freedom of travel for merchants. Chapter 41 stated, “All merchants shall be able to go out of and return to England safely and securely and stay and travel throughout England, as well by land as by water.” It set weights and measures, the basis of the commodity form. Never far from Coke’s thoughts, as he wrote, were “those two great pronouns, *meum* and *tuum*,” possessive pronouns that referred to possessions. As a practical matter, possessions required measurement and thus depended on chapter 35: “Let there be one measure for wine throughout our kingdom, and one measure for ale, and one measure for corn, namely ‘the London quarter’; and one width for cloths whether dyed, russet or halberget, namely two ells within the selvedges. Let it be the same with weights and measures.” The provisions both fleeced and protected the Jews, who had been disarmed and then massacred at the coronation of Richard I, John’s elder brother and predecessor on the throne. As chapter 10 stipulated, “If one who has borrowed from the Jews any sum, great or small, die before that loan be repaid, the debt shall not bear interest while the heir is under age, of whomsoever he may hold; and if the debt fall into our hands, we will not take anything except the principal sum contained in the bond.”

Chapters 28, 30, and 31 put a stop to the robberies of petty tyrants. “No constable or other bailiff of ours shall take anyone’s corn or other chattels unless he pays on the spot in cash for them.” The etymology of the word *chattels* recapitulates the evolution of the commodity and in this case suggests the change from a pastoral to an agrarian economy. “No sheriff, or bailiff of ours, or anyone else shall take the horses or carts of any freeman for transport work save with the agreement of that freeman.” “Neither

 ~~ [ 31 ] ~~~

we nor our bailiffs will take, for castle or other works of ours, timber which is not ours, except with the agreement of him whose timber it is.”

Other chapters have to be understood in terms of the energy ecology, which was based not on coal or oil but on wood. Chapter 47 said, “All forests that have been made forest in our time shall be immediately disafforested; and so be it done with riverbanks that have been made preserves by us in our time.” To *disafforest* meant to remove from royal jurisdiction; it did not mean to clear-cut timber or destroy the trees. Chapter 48 said, “All evil customs connected with forests and warrens, foresters and warreners, sheriffs and their officials, riverbanks and their wardens shall immediately be inquired into in each county by twelve sworn knights of the same county who are to be chosen by good men of the same county and within forty days of the completion of the inquiry shall be utterly abolished by them so as never to be restored.” It refers to the common rights of the *forest*. The physical forest was woodlands; the legal forest was a royal domain under forest law where the king kept deer. Both the word and the law came to England with William the Conqueror.

If noticed at all as part of Magna Carta, chapters 47 and 48 are often discarded as feudal relics, English peculiarities, or irrelevancies of the heritage industry. Yet if we see woodlands as a hydrocarbon energy reserve, we may be willing to give the subject more than a condescending dismissal. We need to adopt a “subsistence perspective.”[^12] “In an age when the primeval

 ~~ [ 32 ] ~~~

instinct of foraging was nearer to the surface than it is today,” wrote Marc Bloch, the great scholar of the Middle Ages, “the forest had greater riches to offer than we perhaps appreciate. People naturally went there for wood, a far greater necessity of life than in this age of oil, petrol, and metal; wood was used for heating and lighting (in torches), for building material (roof slats, castle palisades), for footwear (sabots), for plough handles and various other implements, and as faggots for strengthening roadways.”[^13]

“Grey, gnarled, low-browed, knock-kneed, bowed, bent, huge, strange, long-armed, deformed, hunchbacked, misshapen oakmen.” This is a personification of the massive trunks and small crowns of the ancient oaks of Staverton. The English oak remains where millennia of cattle, goat, and deer ate its more ed- ible competitors. The grazing determines what species thrive. Old trees are the result not of the wildwood (of the Ice Age thirteen millennia earlier) but of wooded pasture. The wooded pas- ture is a human creation, through centuries of accumulated woodsmanship, whose attributes include the *coppice* (which grows again from the stump)—ash and elm provide indefinite succession of crops of poles (for making rakes, scythe handles, surplus used for stakes and firewood); the sucker (which grows again from the root system)—aspen, cherry forming a patch of genetically identical trees called a clone; and the *pollard*—these are cut six to fifteen feet above the ground, leaving a permanent

 ~~ [ 33 ] ~~~

![](https://i.imgur.com/cyuHllC.png)
*Figure 1. A multi-use enclosure at Runnymede showing cattle grazing and tree pollards. Photo by the author.*

trunk called a bolling with sprouts like coppice but out of reach of the livestock.[^14]

Wooded pasture: same land for trees and grazing animals. Wooded commons: owned by one person, but used by others, the commoners. Usually the soil belonged to the lord while grazing belonged to the commoners, and the trees to either—timber to the lord, and wood to commoners. Whole towns were timber-framed: the strut and beam of cottages, the curved wooden rafters, the oak benches of worship. Then wheels, handles, bowls, tables, stools, spoons, toys, and other implements were all made of
wood. Wood was the source of energy.

 ~~ [ 34 ] ~~~

The growth of state power, the ability to make war, and complaints against the monarchy arose from its power to *afforest*, or place under royal law.[^15] With the Norman conquest came innovations in eating utensils (the fork), a new language (French), new people (Normans, Jews), different animals (wild boar, red deer). William and his Norman conquerors (“a French bastard landing with an armed banditti,” said Tom Paine) bypassed the customs of the forests that had prevailed from Anglo-Saxon times. Forests were not necessarily wooded. “The forest has its own laws, based, it is said, not on the Common Law of the realm, but on the arbitrary decree of the King.”[^16] It was the supreme status symbol of the king, a place of sport. The Domesday Book (1086) shows that only about half of the English settlements possessed woodland. In July 1203 King John instructed his chief forester, Hugh de Neville, to sell forest privileges “to make our profit by selling woods and demising assarts.”[^17] In 1215 there were 143 forests in England. Half of them were wood pasture. Few forests were declared in England after 1216. An authority writes that the principal grievances behind Magna Carta were two, “the malpractices of the sheriff and the extent of the forest.”[^18]

Having summarized the charter’s chapters and having invoked the wooded basis of the material life at the time, we now

 ~~ [ 35 ] ~~~

return to the fate of King John. Scarcely had the mud of Runnymede dried on his boots than John resumed war on the barons and began to plot with the pope against them. Innocent III vacated the charter as null and void and prohibited the king from observing it. Louis, later to become king of France, invaded England at the barons’ invitation in May 1216. King John died in October.

The story of his death became the stuff of legend among the peasant commoners, conveyed by word of mouth and remembered as oral history even by William Morris, the wonderful nineteenth-century craftsman, socialist, and poet, whose version I paraphrase. Fleeing his enemies King John lost all his baggage in an onrushing tide of the sea, and in a foul mood took shelter in Swinestead Abbey, Lincolnshire. “How much is this loaf sold for?” he asked at dinner, and when told one penny he answered, “by God, if I live for one year such a loaf shall be sold for twelve
pence!”

One of the monks nearby heard this and considered that his own hour and time to die had come, and that it would be a good deed to slay so cruel a king and so evil a lord. So he went into the garden and plucked plums and replaced the pits with venom. Then he came before the king and knelt saying, “Sire, by St. Austin, this is the fruit of our garden.” The king looked evilly on him and said, “Eat first, monk!” So the monk ate but changed countenance not one whit. So the king ate too. Presently right before the king’s eyes the monk swelled, turned blue, fell down, and died. Then waxed the king sick at heart, and he also swelled, sickened, and died.

This is history from below, and like history from above (or in between), it must be examined. The herbaria and orchards of the

 ~~ [ 36 ] ~~~

English monasteries, besides being early examples of collective labor, were also progenitors of communal living upon natural resources held in common. Thus, when the monk offered King John a fruit of the garden, it was a fruit in the double sense of both a product of human labor and a product of the earth, rain and sunshine—which belong to all—as the peasants who told this story and as William Morris who repeated it well understood. Plums originated in Byzantium and came to England at the time of Magna Carta with returning crusaders. King John thus suffered a poetic death caused by a kind of biological blow-back.

After the death rattle of John and during the minority of the new king, Henry III, only nine years old, the fate of Magna Carta, indeed its whereabouts, was uncertain. France controlled half of England. The papal legate to England at the time of the death of King John and the coronation of the nine-year-old Henry III was Cardinal Gualo, who had been active in the extirpation of the Albigensians. Henry III granted the Charter of the Forest by the counsel of Gualo (and the English bishops). Did the woods nurture heresy? Was the pope’s principal hunter of heretics brought to England to prevent the growth of heresy by promoting this charter?

“The French invasion saved the Great Charter,” says McKechnie.[^19] It was not until 11 September 1217 that France and England made peace, at an island in the river Thames near Kingston. Barefoot and shirtless, Louis was required to renounce all claim to the English throne, and to restore the Charters of Liberties granted by King John. The treaty put an end to two

 ~~ [ 37 ] ~~~

years of civil war. The Victorian constitutional historian Stubbs concluded of the Treaty of Kingston, “in practical importance, scarcely inferior to the charter itself.”[^20] In contrast to its treaty-like function during the baronial wars, the reissue of the charter in time of peace established it as a basis of government.

Respecting the relationship between the Charter of the Forest and the Magna Carta, Wendover, the leading contemporary chronicler, said King John granted a separate forest charter but Blackstone argues this was unlikely because, among other reasons, the dimensions of the parchment of the great charter were sufficient to add forest clauses. William Blackstone published a scholar’s edition of both, *The Great Charter and the Charter of the Forest* (1759). He was the first to print accurate texts of the charters, as they were known to him. “There is no transaction in the antient part of our English history more interesting and important, than the rise and progress, the gradual mutation, and final establishment of the charters of liberties, emphatically stiled the great charter and charter of the forest; and yet there is none that has been transmitted down to us with less accuracy and historical precision.”[^21]

Blackstone noted that the archbishops of Canterbury and Dublin “apprehend the generality of chapter 48 endangered the very being of all forests declaring that it was not the intention of the parties that the general words of the charter should extend to abolish such customs of the forests, without the existence of

 ~~ [ 38 ] ~~~

which the forests themselves could not be preserved.” The forest clauses settled nothing. They provided grounds for renewal of war. The issue of disafforestation kept Magna Carta alive.[^22]

A charter was a material object with a physical history.[^23] At seventeen and three-quarters inches wide and eighteen and one-quarter inches long, the term *magna carta* is surprising. First used in 1218, it distinguished the charter from the companion, but smaller, Charter of the Forest. We should quote the preface to the second of Coke’s *Institutes of the Laws of England* (1642). “It is called Magna Charta, not that it is great in quantity, for there be many voluminous charters commonly passed, specially in these later times, longer than this is; nor comparatively in respect that it is greater than Charta de Foresta, but in respect of the great importance, and weightiness of the matter, as hereafter shall appeare: and likewise for the same cause *Charta de Foresta* is called *Magna Charta de Foresta*, and both of them are called *Magnae Chartae Libertatum Angliae*”—the great charters of English liberties.[^24] They were published by reading aloud four times a year, at the feast of St. Michael’s, Christmas, Easter, and feast of St. John’s. They were read in Latin certainly, in Norman French translation probably, and in English possibly.

The date 11 September recurs in this study four times alto- gether. First in 1217; second, when the Scot William Wallace defeated England in 1297; third, on that day in 1648 when the English Levellers submitted the Large Petition that called for

 ~~ [ 39 ] ~~~

popular sovereignty, reparations, juries, religious toleration, and the opening of enclosures; and fourth, when the South Sea Company congratulated itself on that day in 1713 for receiving the license (or *asiento*) to trade African slaves to Spanish colonies in America. The date associates the charters with the forest commons, with greater Britain, with the Levellers, and with the slave trade.

The two charters were reissued together in 1225. McKechnie states, “it marked the final form assumed by Magna Carta.”[^25] Subsequently, the two were confirmed together. By 1297 Edward I directed that the two charters become the common law of the land. After a law of Edward III in 1369, the two were treated as a single statute. Both charters were printed together at the commencement of the English *Statutes-at-Large*. Blackstone concludes, “the final and complete establishment of the two charters, of liberties and of the forest, which from their first con- cession under King John in a.d. 1215, had been often endangered, and undergone many mutations, for the space of near a century; but were now fixed upon an eternal basis.”

One of those mutations, occurring between 1215 and 1217, mod- ified chapter 7, “and she shall have meanwhile her reasonable estovers of common.” What are “estovers of common”? Coke explains, “When estovers are restrained to woods, it signifieth housebote, hedgebote, and ploughbote.” Botes do not imply a common wood; they could as well appertain to field or hedgerow. *Firebote* and *hedgebote* are quotas for fuel and fencing; *housebote* and *cartbote* are rights for building and equipment. Coke goes on to say

 ~~ [ 40 ] ~~~

that estovers signify sustenance, aliment, or nourishment. Technically then estovers refer to customary gatherings from the woods; often they refer to subsistence generally. Magna Carta defined limits of *privatization*. In Chapter 33, the clause “Henceforth all fish weirs shall be removed from the Thames and the Medway and throughout all England, except along the seacoast,” refers to the right of fishing in another’s water in common with the owner and others (“in common of piscary”). The UN’s International Covenant on Economic, Social and Cultural Rights declares, “In no
case may a people be deprived of its own means of subsistence.”[^26]

In continent after continent the humble figure of the old woman bent from carrying a burden of sticks that she has gathered from the woodlands has been the quintessential figure of an epoch in reproduction. Her protection is one of the oldest injunctions of written human history from the Mosaic codes onward (“When you reap the harvest in your field and forget a swathe, do not go back to pick it up; it shall be left for the alien, the orphan, and the widow,” Deuteronomy 24:19). Wherever the subject is studied, a direct relationship is found between women and the commons. The feminization of poverty in our own day has become widespread precisely as the world’s commons have been enclosed.

What happened between 1215 and 1217 to cause this clause to be inserted in chapter 7? The answer is war. The civil war continued. France invaded. War was fought by mounted knights,

 ~~ [ 41 ] ~~~

powerful units of war, terrifying, expensive, and ubiquitous. The king wanted to reward followers with endowments and lands in order “to raise men from the dust.” War was fought by crossbowmen; it was fought by sailors; it was fought by many thousands of churls and villeins. Monstrous weapons of mass destruction hurled terror from the sky—the mangonel cast millstones, the trebuchet launched bombards, the catapult threw darts, the ballista (like the crossbow) hurled stones and missiles, and the arbalest discharged all manner of arrows, stones, and bolts. They destroyed cities, blinded soldiers, burned houses, razed towns, maimed and mutilated people without discrimination. War produced death by pestilence, drowning, fire, as well as by direct hits from the sky. War produced widows. The “mutation” (Blackstone) of chapter 7 between 1215 and 1217 reflected this reality.

The assize of Woodstock (1184) permitted the poor to have their estovers, but only under stringent rules. McKechnie comments: “If the rich suffered injury in their property, the poor suffered in a more pungent way: stern laws prevented them from supplying three of their primary needs; food, firewood, and building materials.”[^27] In Somerset complaint was made, “from the poor they take, from every man who carried wood upon his back, sixpence.” In Stratford, a warden took a quarter of wheat “for their having paling for their corn and for collecting dead wood for their fuel in the demesne wood of the lord king.” Sometimes a local tyrant established a veritable reign of terror. Inasmuch as the Charter of the Forest (1217) protected

 ~~ [ 42 ] ~~~

the commons it was also, and to that extent, a prophylaxis from terror.

The Forest Charter’s 1st chapter saved common of pasture for all those “accustomed” to it. The 7th forbade foresters or beadles from taking sheaves of corn or oats, or taking lambs or piglets in lieu of a feudal tax called *scotale*. The 9th chapter provided agistment and pannage to freemen. The 13th stated that every freeman shall have his honey. The 14th chapter said that those who come to buy wood, timber, bark or charcoal and carry it out in carts must pay *chiminage* (a road tax) but those who carry wood, bark, or charcoal on their backs need not pay chiminage.

Coke warns us not to let pass the least crumb or syllable of this law. The substantive customs referred to in Magna Carta are to the wooded realm that supported a material culture whose structures and architectonics were composed of wood, not steel or plastic. Richard Mabey, the incomparable English naturalist, author, and broadcaster, writes of the English woodlands, “More than any other kind of landscape they are communal places, with generations of shared natural and human history inscribed in their structures.”[^28]

*Herbage* is common of pasture, like *agistment*, which permitted livestock to roam in the forest. *Pannage* is the right to let the pigs in to get acorns and beech mast. Assarts and swidden are aspects of arable tillage. Firebote, snap wood, turbary, lops and tops refer to fuel. Estovers, cartbote, and housebote refer to tools and building. Chiminage refers to transportation. The widow’s 

 ~~ [ 43 ] ~~~

estovers of common is thus the phrase that leads us to a completely different world, a world of use values.

J. M. Neeson describes the uses of woods: lops and tops or snap wood for the household, furze and weeds for fodder, bavins or sprays such as bakers and potters wanted for their ovens and kilns. She notes where bean stakes could be found, how hazel was good for sheepfolds, how to assemble a chimney-sweeping brush. The woodlands were a reservoir of fuel; they were a larder of delicacies, a medicine chest of simples and cures.[^29] As for food, hazelnuts and chestnuts could be sold at market; autumn mushrooms flavored soups and stews. Wild chervil, fennel, mint, wild thyme, marjoram, borage, wild basil, tansy made herbs for cooking and healing. Wild sorrel, chicory, dandelion leaves, salad burnet, cats-ear, goats-beard, greater prickly lettuce, corn sow-thistle, fat-hen and chickweed, yarrow, charlock, and goose grass made salads. Elderberries, blackberries, bilberries, barberries, raspberries, wild strawberries, rosehips and haws, cranberries and sloes were good for jellies, jams, and wines.

The medievalist Jean Birrell has described the struggle in the thirteenth century over common rights. She places special emphasis on the range of forest commons. Already they were both old and customary. “Most were long standing, though some were recent; some were precisely defined in writing, but most were defined only by custom.” They were threatened by the economic pressures consequent on the growth of towns and the

 ~~ [ 44 ] ~~~

increase of trade, as woods were cleared and assarts were made. The number of commons increased, the amount of common lands diminished, and the lords of the manors attempted to curtail common rights. Intercommoning and *stints* began to emerge; common law and direct action preserved commons. The men of Stoneleigh, Warwickshire petitioned the king in 1290 that they had lost their estovers and pasturage by manorial assarts and were unable to survive.[^30]

Often Magna Carta refers to *freemen*, and often in subsequent centuries the term has been decried; Mark Twain, for instance, in *A Connecticut Yankee in King Arthur’s Court*, refers to it “as a sarcasm of law and phrase.” Hence it is an imposter, a shadow, a superstition, “echoing from the caves of fame.” However, if we keep in mind the microeconomy of the woods so well described by Neeson, we can appreciate Shelley’s apt reply to the question posed at the beginning of this chapter—“What art thou Freedom?”

> For the labourer thou art bread,  
> And a comely table spread.  
> From his daily labour come  
> To a neat and happy home.  
> 
> Thou art clothes, and fire, and food

So common rights differ from human rights. First, common rights are embedded in a particular ecology with its local

 ~~ [ 45 ] ~~~

husbandry. For commoners, the expression “law of the land” from chapter 39 does not refer to the will of the sovereign. Commoners think first not of title deeds, but of human deeds: how will this land be tilled? Does it require manuring? What grows there? They begin to explore. You might call it a natural attitude. Second, commoning is embedded in a labor process; it inheres in a particular praxis of field, upland, forest, marsh, coast. Common rights are entered into by labor. Third, commoning is collective. Fourth, being independent of the state, commoning is independent also of the temporality of the law and state. Magna Carta does not list rights, it grants perpetuities. It goes deep into human history.

Magna Carta was a treaty among contending forces in a civil war; as J. C. Holt says, it was a political document. It attempted to put to rest seven conflicts, namely between church and monarchy, between individual and the state, between husband and wife, between Jew and Christian, between king and baron, between merchant and consumer, between commoner and privatizer. It did not settle these conflicts in the sense of declaring victory. Its chapter 39 has grown to embody fundamental principles, habeas corpus, trial by jury, prohibition of torture. But its work is far from done. Other chapters, too, must grow. We shall find five further principles in the Charters of Liberties: the principle of neighborhood; the principle of subsistence; the principle of travel; the principle of anti-enclosure; and the principle of reparations.


---
Footnotes:

[^1]: On Magna Carta’s influence see Alan Harding, *A Social History of English Law* (Baltimore: Penguin Books, 1966), 55.
[^2]: Winston Churchill, *The Birth of Britain, vol. 1 of A History of the English Speaking Peoples* (New York: Dodd, Mead, 1956), vii, xvi.
[^3]: Geoffrey Robertson, *Crimes Against Humanity: The Struggle for Global Justice* (New York: New Press, 1999), 2–3. See also Anne Pallister, *Magna Carta: The Heritage of Liberty* (Oxford: Clarendon Press, 1971).
[^4]: Simon Schama, *A History of Britain: At the Edge of the World* (New York: Hyperion, 2000), 65. John Cleese et al., *Monty Python and the Holy Grail* (1975).
[^5]: Silvia Federici, *Caliban and the Witch: Women, The Body, and Primitive Accumulation* (New York: Autonomedia, 2004).
[^6]: Norman Cohn, *The Pursuit of the Millennium: Revolutionary Millenarians and Mystical Anarchists of the Middle Ages*, rev. ed. (New York: Oxford University Press, 1970), 66–71.
[^7]:  Frances Gies and Joseph Gies, *Women in the Middle Ages* (New York: Crowell, 1978), 28.
[^8]: Reinhold Röricht, “Ordinacio de predicatione S. Crucis, in Anglia,” in
*Quinti belli sacri scriptores* (Geneva, 1879); quoted in James M. Powell, *Anatomy
of a Crusade*, 1213–1221 (Philadelphia: University of Pennsylvania, 1986), 52.
[^9]: The minstrel Sarrazin’s *Histoire des ducs de Normandie et des rois d’An-
gleterre*, though composed in 1220, was not published until 1840. The four types
of disparaged husbands were lunatics, villeins, cripples, and the impotent.
[^10]:  J. C. Holt, *Magna Carta*, 2nd ed. (Cambridge: Cambridge University Press, 1992), 46.
[^11]: Federici, *Caliban and the Witch*, see chap. 1. See also Terisa E. Turner
and Leigh S. Brownhill, eds., “Gender, Feminism and the Civil Commons,” *Canadian Journal of Development Studies* 22 (2001), a significant collection of articles.
[^12]: Maria Mies and Veronika Bennholdt-Thomsen, *The Subsistence Perspective: Beyond the Globalised Economy*, trans. Patrick Camiller, Maria Mies, and Gerd Wieh (New York: Zed, 1999).
[^13]: Marc Bloch, *French Rural History*, trans. Janet Sondheimer (Berkeley: University of California Press, 1966), 6.
[^14]: Oliver Rackham, *The History of the Countryside* (London: J. M. Dent, 1986), 66.
[^15]: Nancy Lee Peluso and Peter Vandergeest, “Genealogies of the Political Forest and Customary Rights in Indonesia, Malaysia, and Thailand,” *Journal of Asian Studies* 60, no. 1 (August 2001): 761–812.
[^16]: J. R. Maddicott, “Magna Carta and the Local Community,” *Past &
Present* 102 (February 1984): 37, 72.
[^17]: Holt, *Magna Carta*, 52.
[^18]: Maddicott, “Magna Carta,” 27.
[^19]: William Sharp McKechnie, *Magna Carta: A Commentary on the Great Charter of King John* (Glasgow: J. Macklehose and Sons, 1914), 141.
[^20]: William Stubbs, *The Constitutional History of England* (Oxford: Clarendon Press, 1894), 2:25.
[^21]: The originals of the Charter of the Forest are in the Bodleian Library, Oxford, and in Durham Cathedral, the regent’s seal in green, the papal legate’s in yellow.
[^22]: Holt, *Magna Carta*, 275.
[^23]: Four originals remain preserved, one in Salisbury Cathedral, another in Lincoln Cathedral, and the others in the British Library.
[^24]: Holt, *Magna Carta*, 18.
[^25]: McKechnie, *A Commentary*, 415.
[^26]: Gerrard Winstanley, *Works*, ed. George H. Sabine (Ithaca: Cornell University Press, 1941), 519; and *International Covenant on Economic, Social and Cultural Rights* (1966, 1976), pt. 1, art. 1, chap. 2. Edward Coke, *The Second Part of the Institutes of the Laws of England* (London: W. Clarke and Sons, 1809), 17.
[^27]: McKechnie, *A Commentary*, 426.
[^28]: Gareth Lovell Jones and Richard Mabey, *The Wildwood: In Search of Britain’s Ancient Forests* (London: Aurum Press, 1993).
[^29]: J. M. Neeson, *Commoners: Common Right, Enclosure and Social Change in England, 1700–1820* (New York: Cambridge University Press, 1993), 158–59.
[^30]: Jean Birrell, “Common Rights in the Medieval Forest: Disputes and Conflicts in the Thirteenth Century,” *Past & Present*, no. 17 (1987): 48. In 1970 E. P. Thompson used to speed through this village in his Land Rover on his way to work at Warwick.


<script type="application/json" class="js-hypothesis-config">
{
"showHighlights": false
}
</script>
<script src="https://hypothes.is/embed.js" async></script>
