.. title: Experimental Publishing I – Critique, Intervention, And Speculation
.. date: 2019-04-01 22:44
.. slug: experimental-publishing-1
.. author: ...

A half-day symposium with talks by Rebekka Kiesewetter and Eva Weinmayr (AND Publishing/Valand Academy)  

**![](https://www.post-publishing.org/wp-content/uploads/2019/03/Screen-Shot-2019-03-12-at-6.17.38-PM-212x300.png)1-5pm April 11**  
**Centre for Postdigital Cultures**  
**Teaching Room**  
**3rd Floor Lanchester Library**  
**Coventry University**  
**Registration (free):** [https://experimentalpublishing.eventbrite.co.uk](https://experimentalpublishing.eventbrite.co.uk/)

This is the first in a series of symposia hosted by the Centre for Postdigital Cultures (CPC) exploring contemporary approaches to experimental publishing. Over the course of the series, we will ask questions about the role and nature of experimentation in publishing, about ways in which experimental publishing has been formulated and performed in the past, and ways in which it shapes our publishing imaginaries at present. This series aims to conceptualise and map what experimental publishing is or can be and to explore what lies behind our aims and motivations to experiment  _through_ publishing. As such, it forms the first activity within the CPC’s new  [Post-Publishing programme](https://post-publishing.org/), an initiative committed to exploring iterative and processual forms of publishing and their role in reconceptualising publishing as an integral part of the research and writing process, i.e. as that which inherently shapes it.

**Speakers**

[Rebekka Kiesewetter](https://www.rebekkakiesewetter.com/)  holds a Lic. phil. I (MA) in art history, economics and modern history from the University of Zurich. Her works in critical theory, practice and making as critique focus on the intersections of experimental publishing, architecture, arts, artistic research, and the humanities.

[Eva Weinmayr](http://evaweinmayr.com/) is an artist, educator, researcher and writer based in London and Gothenburg investigating the border crossings between contemporary art, radical education, and institutional analysis by experimenting with modes of intersectional feminist knowledge practices. She currently conducts a PhD on micro-politics of publishing at Valand Academy, University of Gothenburg and runs together with artist Rosalie Schweiker AND Publishing, a feminist publishing practice based in London.

![](https://www.post-publishing.org/wp-content/uploads/2019/03/AND-Publishing-The-Piracy-Reader-2014.png)

AND Publishing, The Piracy Reader, 2014

**Concept**

Experimental publishing can be positioned as an intervention, a mode of critique, and a tool of speculation. It is a way of thinking about writing and publishing today that has at its centre a commitment to questioning and breaking down distinctions between practice and theory, criticality and creativity, and between the scholarly and the artistic.

In this series of events we propose to explore contemporary approaches to experimental publishing as:

-   _an ongoing critique_  of our current publishing systems and practices, deconstructing existing hegemonies and questioning the fixtures in publishing to which we have grown accustomed—from the book as a stable object to single authorship and copyright.
-   _an affirmative practice_  which offers means to re-perform our existing writerly, research, and publishing institutions and practices  _through_  publishing experiments.
-   _a speculative practice_  that makes possible an exploration of different futures for writing and research, and the emergence of new, potentially more inclusive forms, genres, and spaces of publishing, open to ambivalence and failure.

This take on experimentation can be understood as a heterogeneous, unpredictable, and uncontained  _process_, one that leaves the critical potentiality of the book as a medium open to new intellectual, political, and economic contingencies.

<br>
<br>
---
