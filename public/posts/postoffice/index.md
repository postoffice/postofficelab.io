.. title: Welcome to the Post Office
.. date: 2019-05-08 22:47
.. slug: postoffice
.. author: ...

Post Office is an exercise in imagining counter-institutions in response to the crisis of those very institutions. It's an endeavour in devising practices that collectively re-configure the public infrastructures against the onslaught of managerial neoliberalism and technological acceleration. As a research collective (connected to the [Centre for Postdigital Cultures at Coventry University](http://www.coventry.ac.uk/research/areas-of-research/postdigital-cultures/)), it is a horizontal experiment in knowledge production and action situated between theory and technology, politics and poetics, the inside and the outside of the university.

Here you can find out about [us](/pages/po/), our [conceptual framework](/pages/post/), [commitments](/pages/poethics/), [events and news](/pages/postings/), [projects](/pages/postfolio/), [technosocial workflows we develop](/pages/postscripts/) and [our Post Office Press](/pages/pop/).