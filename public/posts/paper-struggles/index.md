.. title: Paper Struggles & Public Library and the Property Form
.. date: 2019-11-27 12:00:00 UTC
.. slug: paper-struggles
.. previewimage: /images/rameshwari_DU_protest.jpg
.. author: ...

**An exhibition and seminar at:**
**Raven Row**, 56 Artillery Lane, Spitalfields, London, E1 7LS
**Exhibition, 'Paper Struggles', opening:** Monday, 9 December, 18.30-21.00 **Continues:** Tuesday 10 and Wednesday 11 December, 11.00-21.00

**Seminar, 'Public Library and the Property Form':** Tuesday 10 December, 10.30-13.30
**Guest speakers:** Balász Bodó & Nanna Bonde Thylstrup
**Seminar registration:** http://tiny.cc/public_library

## 'Paper Struggles'

TThis exhibition documents how struggles over access to knowledge in the digital realm are reflected in the world of print and paper. The digital access has expanded the volume of available text exponentially since to the days of print. Yet, paper remains the preferred format of reading for many students, scholars and researchers across the globe. Copying on paper is often the most affordable way of obtaining texts. Struggles over access can thus be understood as struggles over (the abundance of) paper. The status of paper in the digital age serves as a starting point for the exhibition, which tells a story about the uneven and messy world of knowledge today.

The exhibition features five works:

1. Rameshwari Photocopy Services legal case
2. Kenneth Goldsmith: 'Printing out the Internet'
3. Monoskop: 'Architecture' & 'Anthropocene'
4. 'Piracy Project', a collaboration between Andrea Francke and Eva Weinmayr
5. 'Memory of the World, Catalogue by Slowrotation'

Conceived by Marcell Mars and Tomislav Medak, on the invitation of Kaja Marczewska.

We wish to thank: Kathrin Bonnar, Toby Boundy, Ross Downes, Alex Sainsbury, David Williams, Rosemary Grennan, MayDay Rooms, Lawrence Liang, Rabindra Patra, Shubigi Rao, Mohammad Salemy, Dušan Barok, Kenneth Goldsmith, Andrea Francke & Eva Weinmayr.


## 'Public Library and the Property Form'

This seminar will explore how intellectual property in the digital realm has affected the institution of the public library, and its aim of providing access to knowledge to all members of society. While the Internet has enabled a massive expansion of access to all kinds of publications, libraries were initially and remain severely limited in extending to digital ‘objects’ the de-commodified access they provide in the world of print. Consequently, the centrality of libraries in facilitating, organising and disseminating literature and science has faded. Thus, while a transition to the digital has provided opportunities to reconsider how societies produce, sustain and make available literature and science, incumbent interests combined with a property-form that treats intellectual creation as if it were a piece of land, have resisted the transformation of our systems of cultural production. Given this context, readers who have been denied access to information due to territorial, institutional and economic barriers have created their own systems of access through the sharing of PDFs and shadow libraries, doing what public libraries are not allowed to do.

In this seminar we want to take stock of the present and future role of libraries in publishing texts, supporting universal access to information, and advocating the radical social and economic imaginaries needed to change the status quo.

### Schedule

10:30 Marcell Mars & Tomislav Medak: 'Public Library and the Property Form'
11:00 Balász Bodó: 'Is the Open Knowledge Commons Idea a Curse in Disguise? Towards Sovereign Institutions of Knowledge.', respondent: Janneke Adema
12:00 break
12:15 Nanna Bonde Thylstrup: 'Gleaning Knowledge: The Infrapolitics of Shadow Libraries', respondent: Gary Hall
13:15 Discussion, introduction: Kaja Marczewska

The seminar is moderated by Valeria Graziano.

### Speakers

**Balazs Bodo** is an Associate Professor at the University of Amsterdam, Institute of Information Law. He is interested in conflicts around freedom, which take place at the intersection of digital technologies and the law. He is currently leading an ERC project on the regulation of decentralised technologies.

**Nanna Bonde Thylstrup** is Associate Professor of Communication and Digital Media at Copenhagen Business School. She is interested in how media theory, cultural theory and critical theory can unpack and unfold issues related to datafication and digitisation. She is the author of *The Politics of Mass Digitisation* published by MIT Press (2019) and has co-edited *Uncertain Archives* (forthcoming).
